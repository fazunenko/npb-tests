Tasks:
1) BE:
Develop automatic tests for the https://bitly.com service
The tests must be based on the 'supertest' framework (https://www.npmjs.com/package/supertest) and cover the following methods:
  /v3/user/info
  /v3/user/link_history
  /v3/shorten
API could be found at http://dev.bitly.com/api.html
Both positive and negative cases must be covered

2) FE:
Verify site http://firepoker.io/#/
  1. Crete 5-7 functional test cases (English)
  2. The same test cases translate to Gherkin
  3. Attempt to implement part of that cases at JS (Protractor) using page objects




