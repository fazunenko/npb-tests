
const request = require('supertest');
const assert = require('chai').assert
const expect = require('chai').expect
const hostUrl = 'https://api-ssl.bitly.com'

const HOST = request(hostUrl)
// login and token are hardcoded for the sake of simplicity
// curl -u "email:pswd" -X POST "https://api-ssl.bitly.com/oauth/access_token"

const LOGIN='fdafda1'
const TOKEN='927f3eba6b740dd66b2aa848f916a449633e6a1e'
const CLIENT_ID = 'ece654beaf35f9c29f610ffd4fb128702b4bad15'
const TOTAL_LINKS = 3; // how many links have been shorten

const LOGIN_2='fdafda2'
const TOKEN_2='4ec8400da709251f26acd775685e724df3c56785'


describe('/v3/user/info: Return or update information about a user', function() {
    var info = '/v3/user/info?access_token=' + TOKEN
    it('default params', function (done) {
        HOST.get(info)
            .expect(200)
            .end(function (err, res) {
                checkStatus(res, 200, "OK")
                checkDataProperties(res.body.data)
                done()
            })
    })

    it('login - (optional) the bitly login of the user whose info to look up. ' +
        'If not given, the authenticated user will be used.', function(done) {
        HOST.get(info + '&login=' + LOGIN)
            .expect(200)
            .end(function (err, res) {
                checkStatus(res, 200, "OK")
                checkDataProperties(res.body.data)
                done()
            })

    })

    it('full_name - (optional) set the users full name value. ' +
        '(only available for the authenticated user.)', function(done) {
        // name is different each time
        var name = 'Name+' + new Date().getTime()
        HOST.get(info + '&full_name=' + name)
            .expect(200)
            .end(function (err, res) {
                checkStatus(res, 200, "OK")
                checkDataProperties(res.body.data)
                expect(res.body.data.full_name).to.equal(name.replace('+', ' '))
                done()
            })
    })


    it('negative: bad login', function(done) {
        HOST.get('/v3/user/info?login=fdafda1@mail.ru&access_token=' + TOKEN)
            .expect(200)
            .end(function (err, res) {
                checkStatus(res, 500, "INVALID_ARG_LOGIN")
                expect(res.body.data).to.equal(null)
                done()
            })

    })

    it('negative: correct login, no access_token', function(done) {
        HOST.get('/v3/user/info?login=' + LOGIN)
            .expect(200)
            .end(function (err, res) {
                checkStatus(res, 500, "MISSING_ARG_ACCESS_TOKEN")
                expect(res.body.data).to.equal(null)
                done()
            })

    })
})


describe('/v3/user/link_history: Returns entries from a users ' +
    'link history in reverse chronological order.', function() {

    var link_history = '/v3/user/link_history?access_token=' + TOKEN

    it('default', function(done) {
        HOST.get(link_history)
            .expect(200)
            .end(function (err, res) {
                checkStatus(res, 200, "OK")
                checkHistory(res.body.data)
                done()
            })
    })

    var parameters_to_select_all = [
        'created_before=' + new Date(20000000000).getTime(),
        'created_after=' + new Date(100000000).getTime(),
        'modified_after=' + new Date(10000000).getTime(),
    ]
    parameters_to_select_all.forEach(function (p) {
        it(p, function(done) {
            HOST.get(link_history + '&' + p)
                .expect(200)
                .end(function (err, res) {
                    checkStatus(res, 200, "OK")
                    checkHistory(res.body.data)
                    done()
                })
        })

    })

    var parameters_to_select_none = [
        'created_before=' + new Date(100000).getTime(),
        'created_after=' + new Date(200000000000).getTime(),
        'modified_after=' + new Date(10000000000).getTime(),
    ]
    parameters_to_select_none.forEach(function (p) {
        it(p, function(done) {
            HOST.get(link_history + '&' + p)
                .expect(200)
                .end(function (err, res) {
                    checkStatus(res, 200, "OK")
                    checkHistorySize(res.body.data, 0)
                    done()
                })
        })

    })

    var limits = [-100, 0, 1, 2, 3, 1000]
    limits.forEach(function (p) {
        it('limit=' + p, function(done) {
            HOST.get(link_history + '&limit=' + p)
                .expect(200)
                .end(function (err, res) {
                    checkStatus(res, 200, "OK")
                    expect(res.body.data.result_count).to.equal(TOTAL_LINKS)
                    var expected = Math.min(3, Math.max(p, 1)) // 1 <= expected <= 3
                    expect(res.body.data.link_history.length).to.equal(expected)
                    done()
                })
        })

    })

    var TODO = ['link', 'offset', 'expand_client_id', 'archived', 'link_deeplinks',
     'domain_deeplinks', 'keyword', 'encoding_login', 'campaign_id', 'query']
    TODO.forEach(function (t) {
        it.skip(t + ' TODO', function () {
        })
    })


})


describe('/v3/shorten: Returns entries from a users ' +
    'Given a long URL, returns a Bitlink.', function() {

    var shorten = '/v3/shorten?access_token=' + TOKEN_2
    var exampleURL = 'http://betaworks.com/'
    var exampleShorten = 'http://bit.ly/2j1nuI6'

    it('longUrl - a long URL to be shortened (example: http://betaworks.com/)', function(done) {
        HOST.get(shorten + '&longUrl=' + encodeURIComponent(exampleURL))
            .expect(200)
            .end(function (err, res) {
                checkStatus(res, 200, "OK")
                checkURL(res.body.data, exampleURL)
                expect(res.body.data.url).to.equal(exampleShorten)
                done()
            })
    })

    var TODO = [
        'TODO: shorten URL points to the same URL',
        'TODO: for different URLs shorten URL is different',
        'TODO: shorten URL is reflected in the link_history']
    TODO.forEach(function (t) {
        it.skip(t + ' TODO', function () {
        })
    })


    var formats = ['json', 'txt', 'xml', 'unknown']
    formats.forEach(function (format) {
        it('format=' + format, function(done) {
            HOST.get(shorten + '&longUrl=' + encodeURIComponent(exampleURL) + '&format=' + format)
                .expect(200)
                .end(function (err, res) {
                    checkFormat(res.text, exampleURL, exampleShorten, format)
                    done()
                })

        })
    })

    var domains = ['bit.ly', 'j.mp', 'bitly.com']
    domains.forEach(function (domain) {
        it('domain=' + domain, function(done) {
            HOST.get(shorten + '&longUrl=' + encodeURIComponent(exampleURL) + '&domain=' + domain)
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.data.url).to.equal(exampleShorten.replace('bit.ly', domain))
                    done()
                })

        })
    })

    var domainsNotSupported = ['us.gov', 'rbc.ru', 'hello_world']
    domainsNotSupported.forEach(function (domain) {
        it('negative domain=' + domain, function(done) {
            HOST.get(shorten + '&longUrl=' + encodeURIComponent(exampleURL) + '&domain=' + domain)
                .expect(200)
                .end(function (err, res) {
                    checkStatus(res, 500, "INVALID_ARG_DOMAIN")
                    expect(res.body.data.length).to.equal(0)
                    done()
                })

        })
    })

    var invalidURLs = ['us.gov', 'http://', 'https://com', 'https:/example.com']
    invalidURLs.forEach(function (longUrl) {
        it('negative invalid long url ' + longUrl, function(done) {
            HOST.get(shorten + '&longUrl=' + encodeURIComponent(longUrl))
                .expect(200)
                .end(function (err, res) {
                    checkStatus(res, 500, "INVALID_URI")
                    expect(res.body.data.length).to.equal(0)
                    done()
                })

        })
    })

})

describe('negative tests for incorrect/missed token', function() {

    var methods = ['/v3/user/info?', '/v3/user/link_history?', '/v3/shorten?']
    methods.forEach(function (method) {
        it('negative: no access_token ' + method, function(done) {
            HOST.get(method)
                .expect(200)
                .end(function (err, res) {
                    checkStatus(res, 500, "MISSING_ARG_ACCESS_TOKEN")
                    if (method.includes('shorten')) {
                        expect(res.body.data.length).to.equal(0)
                    } else {
                        expect(res.body.data).to.equal(null)
                    }

                    done()
                })

        })

        it('negative: bad access_token' + method, function(done) {
            HOST.get(method + 'access_token=123456')
                .expect(200)
                .end(function (err, res) {
                    if (method.includes('shorten')) {
                        checkStatus(res, 500, "INVALID_ARG_ACCESS_TOKEN")
                        expect(res.body.data.length).to.equal(0)
                    } else {
                        checkStatus(res, 403, "INVALID_ACCESS_TOKEN")
                        expect(res.body.data).to.equal(null)
                    }
                    done();
                })

        })

    })

})




// utility functions


function checkStatus(res, num, txt) {
    expect(res.body.status_code).to.equal(num)
    expect(res.body.status_txt).to.equal(txt)
}

function checkHasProperties(data, props) {
    props.forEach(function (p) {
        expect(data).to.have.property(p)
    })
}

function checkDataProperties(data) {
    const INFO_PROPERTIES = [
        'login', 'profile_url', 'profile_image',
        'member_since', 'full_name', 'display_name',
        'share_accounts',

        'apiKey', 'is_enterprise', 'has_master',
        'has_password', 'custom_short_domain', 'tracking_domains',
        'default_link_privacy'

    ]
    checkHasProperties(data, INFO_PROPERTIES)
    expect(data.login).to.equal(LOGIN)
    expect(data.profile_url).to.equal("http://bitly.com/u/" + LOGIN)
    expect(data.member_since).to.equal(1509690694)
    expect(data.apiKey).to.equal("R_4f6759103cf5488bbea1de5b62661324")
}


function checkHistory(data) {
    checkHistorySize(data, TOTAL_LINKS)
    data.link_history.forEach(function (h) {
        checkHistoryElement(h)
    })
}

function checkHistorySize(data, expectedCount) {
    expect(data.result_count).to.equal(expectedCount)
    expect(data.link_history.length).to.equal(expectedCount)
}


function checkHistoryElement(data) {

    const HISTORY_PROPERTIES = [
        'link', 'aggregate_link', 'long_url', 'archived',
        'created_at', 'user_ts', 'modified_at', 'title',
        'client_id', 'encoding_user'
    ]
    checkHasProperties(data, HISTORY_PROPERTIES)
    expect(data.client_id).to.equal(CLIENT_ID)
    expect(data.encoding_user.login).to.equal(LOGIN)
    expect(data.created_at).to.be.at.most(data.modified_at)
}

function checkURL(data, url) {

    const SHORTEN_PROPERTIES = [
        'url', 'long_url', 'hash', 'global_hash', 'new_hash'
    ]
    checkHasProperties(data, SHORTEN_PROPERTIES)
    expect(data.long_url).to.equal(url)
    expect(data.global_hash).to.not.equal(null)
    assert(data.url.endsWith(data.hash))
}

function checkFormat(text, long_url, shorten_url, format) {
    switch (format) {
        case 'txt':
            assert(text.startsWith(shorten_url))
            break
        case 'xml':
            assert(text.startsWith('<?xml version="1.0" encoding="UTF-8"?>'))
            assert(text.indexOf(long_url) > 0)
            break
        default:
            assert(text.startsWith('{"status_code":200,"status_txt":"OK"'))
            assert(text.indexOf(long_url) > 0)
    }
    assert(text.indexOf(shorten_url) >= 0)
}
