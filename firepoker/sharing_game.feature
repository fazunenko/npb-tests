Feature: Sharing created game
  Moderator could invite people to participate.
  To participate one should know the Game-URL

  Scenario: everyone see all connected people
    Given Moderator created new game and shared the Game-URL with User
     Then User opens Game-URL
      And sees game name, moderator name, 2 participants
     Then Moderator sees 2 participants
     Then User opens another URL
     Then Moderator sees 1 participant
     Then User opens another Game-URL again

