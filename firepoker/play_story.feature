Feature: Playing story
  A story is played until all participants played their cards.
  A card played by a participant is not visible to others.
  When all cards are played moderator sees the average estimate.
  Moderator can adjust the estimate
  Moderator can either accept the round, or obligate participants to
  play the round again or cancel the round.
  
  Scenario: single player game
   Given New game just created with 3 stories
    Then I click 'Add story'
    Then I check selected story is displayed
     And all cards enabled
     And 'Accept round' and 'Cancel round' are enabled
     And 'Play again' and 'Reveal cards' are disabled
    Then I click a card
     And check the selected card is displayed
     And all cards disappeared
     And 'Accept round' , 'Play again', 'Reveal cards' ,'Cancel round' are enabled
    Then I click 'Accept round'
     And check page content


