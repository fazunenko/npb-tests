
const URL = 'http://firepoker.io/#/'
describe('Create a new game', function() {
    it('"Create a new game" page contains all necessary elements', function() {
        browser.get(URL)
        element(by.css('a.btn.btn-default.btn-lg')).click()
        expect(element.all(by.css('div.form-group')).count()).toEqual(5)
        expect(element(by.css('.btn.btn-success')).isEnabled()).toBe(false)
    })
})


