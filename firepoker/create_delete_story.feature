Feature: Creating/deleting stories
   A store could be created in three different way:
     - during game declaration
     - as Structured story
     - as Free-form story
   Not played stored are displayed in the 'Store list'
   A not played store could be deleted from that list
   Played stories are displayed with given estimates
   and could be deleted.

   Given New game just created with 3 stories
    Then I click 'Story list' tab
     And see 3 stories entered
    Then I click 'delete story icon'
     And see 2 stories left
    Then I click 'Free-form' tab
     And see 'Add story' is disable
    Then I enter Story and notes
    Then I click 'Add story'
    Then I click 'Cancel round'
     And see 3 stories present
    Then I click 'Structured' tab
     And see 'Add story' is disable
    Then I enter all fields
    Then I click 'Add story'
    Then I click 'Cancel round'
    Then I click 'Story list'
     And see 4 stories present
