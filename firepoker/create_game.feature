Feature: Create a new game
  A new game could be created by anyone.
  To create a new game one should provide his(her) name and
  name of the game. The person who created a game becomes moderator.
  Optionally one could provide game description and stories.
  (Stories could be added later)
  One could also alter set of estimate cards.

  Scenario: 'Create a new game' page contains all necessary elements
    Given 'Create a new game' page just opened
     Then I should see all expected textareas present
     Then 'Create game' button is present and disable
     Then I click 'Cancel' and see the firepoker home page

  Scenario: To create new game I need to provide at least my name and game name
    Given 'Create a new game' page just opened
     Then I enter my name
     Then 'Your name' check 'Create game' is disable
     Then I clear my name
      And I enter 'Game name'
     Then I check 'Create game' is disable
     Then 'Your name' check 'Create game' is enable
     Then I click 'Create game'
     Then I check the page to contain entered info

  Scenario: All information provided during game creation is visible
    Given 'Create a new game' page just opened
     Then I enter all fields
     Then I alter default card set
     Then I click 'Create game'
     Then the page should contain all the entered info
