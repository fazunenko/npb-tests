Solution:

Note: There is no specification how the game should work, so all my test cases are based
on the assumption that what I see is working correct.

Note2: All scenarios by default start with opening 'http://firepoker.io/#/' page
and clicking "Play now" button (aka 'open page')


Case-1) Verify the 'Create a new game' page content
- open 'Create a new game' page
- check text of all labels
- check that all expected textareas present
- check 'Choose the set of cards' content
- check 'Create game' button is present and disable
- click 'Cancel' and expect to see the home page

Case-2) Verify creating new game with minimal input
- open 'Create a new game' page
- check 'Create game' is disable
- enter 'Your name' check 'Create game' is disable
- clear 'Your name' and enter 'Game name' check 'Create game' is disable
- enter 'Your name' check 'Create game' is enable
- click 'Create game'
- check the page to contain entered info


Case-3) Verify creating new game when all fields are filled out
- open 'Create a new game' page
- enter all fields and click 'Create game'
- check the page to contain entered info

Case-4) Creating/deleting stories
- open 'Create a new game' page
- enter moderator/game names
- enter 3 lines in "import stories" and click 'Create game'
- click 'Story list' tab
- check 3 stories present
- click 'delete story icon'
- check 2 stories present
- click 'Free-form' tab
- check 'Add story' is disable
- enter Story and notes click 'Add story'
- click 'Cancel round'
- check 3 stories present
- click 'Structured' tab
- check 'Add story' is disable
- enter all fields
- click 'Add story'
- click 'Cancel round'
- click 'Story list'
- check 4 stories present

Case-5) Playing story
- open 'Create a new game' page
- enter moderator/game names and click 'Create game'
- enter fields and click 'Add story'
- check selected story is displayed
- check all cards enabled
- check 'Accept round' and 'Cancel round' are enabled
- check 'Play again' and 'Reveal cards' are disabled
- click a card
- check the selected card is displayed
- check all cards disappeared
- check 'Accept round' , 'Play again', 'Reveal cards' ,'Cancel round' are enabled
- click 'Accept round'
- check page content


Some other cases that could be checked by a single user:
- selecting different set of cards affect on the cards to be displayed
- played stories are not displayed in the story list
- canceled stories appeared in the story list
- 'Play again' button behavior
- 'Accept round' when no card is selected causes '0' estimation
- Selected estimation could be adjusted by moderator


Case-6) Verify sharing page
- browser1: open 'Create a new game' page
- browser1: enter minimal required data to create new game, click create game
- browser2: open Game-URL
- browser2: check content (game name, moderator name, participants
- browser1: check 'Participants (2)'
- browser2: open another URL
- browser1: check 'Participants (1)'
- browser2: open Game-URL
- browser1: check 'Participants (2)'

Other cases to be verified in multiuser environment
- new story added by moderator appears in all participants windows
- canceling round make the story to disappear
- after opening Game-URL all the played stories are displayed
- playing a card: the card is displayed as closed for all participants but current
- a participant may play just one card per story
- cards become open when all participants played a card
- 'Reveal cards' pressed by moderator make all cards open, but round is still playing
- when all cards played default estimation displayed to the moderator is an average of all estimates
- the estimate become visible to others only after the moderator clicked 'Accept round'


